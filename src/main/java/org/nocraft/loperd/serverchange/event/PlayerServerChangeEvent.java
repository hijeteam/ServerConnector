package org.nocraft.loperd.serverchange.event;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;

public class PlayerServerChangeEvent extends Event implements Cancellable {

    private final ProxiedPlayer player;
    private final ServerInfo from;
    private final ServerInfo target;
    private boolean cancel = false;
    private long delay = 0L;

    public PlayerServerChangeEvent(ProxiedPlayer player, ServerInfo from, ServerInfo target) {
        this.player = player;
        this.from = from;
        this.target = target;
    }

    public ProxiedPlayer getPlayer() {
        return this.player;
    }

    public ServerInfo getFrom() {
        return this.from;
    }

    public ServerInfo getTarget() {
        return this.target;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public long getDelay() {
        return this.delay;
    }

    @Override
    public boolean isCancelled() {
        return this.cancel;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }
}
