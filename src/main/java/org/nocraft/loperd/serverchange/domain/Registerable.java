package org.nocraft.loperd.serverchange.domain;

public interface Registerable {

    void register();

    void unregister();
}
