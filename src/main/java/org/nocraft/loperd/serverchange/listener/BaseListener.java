package org.nocraft.loperd.serverchange.listener;

import net.md_5.bungee.api.plugin.Listener;
import org.nocraft.loperd.serverchange.domain.Registerable;
import org.nocraft.loperd.serverchange.ServerConnectorPlugin;
import org.nocraft.loperd.serverchange.domain.Shutdownable;

public abstract class BaseListener implements Registerable, Shutdownable, Listener {
    private ServerConnectorPlugin plugin;

    public BaseListener(ServerConnectorPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void register() {
        this.plugin.registerEvents(this);
    }

    @Override
    public void unregister() {
        this.plugin.unregisterEvents(this);
    }

    @Override
    public void shutdown() {
        this.plugin = null;
    }
}
