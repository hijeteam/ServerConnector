package org.nocraft.loperd.serverchange;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import org.nocraft.loperd.serverchange.command.BaseCommand;
import org.nocraft.loperd.serverchange.command.SendCommand;
import org.nocraft.loperd.serverchange.command.ServerCommand;
import org.nocraft.loperd.serverchange.domain.Composer;
import org.nocraft.loperd.serverchange.event.PlayerServerChangeEvent;
import org.nocraft.loperd.serverchange.listener.BaseListener;

import java.util.concurrent.TimeUnit;

public final class ServerConnectorPlugin extends Plugin {

    private final Composer<BaseCommand> commands = new Composer<>();

    @Override
    public void onEnable() {
        this.commands.add(new ServerCommand(this));
        this.commands.add(new SendCommand(this));
        this.commands.register();
    }

    @Override
    public void onDisable() {
        this.commands.unregister();
        this.commands.shutdown();
    }

    public void registerEvents(BaseListener listener) {
        getProxy().getPluginManager().registerListener(this, listener);
    }

    public void unregisterEvents(BaseListener listener) {
        getProxy().getPluginManager().unregisterListener(listener);
    }

    public void registerCommand(BaseCommand command) {
        this.getProxy().getPluginManager().registerCommand(this, command);
    }

    public void unregisterCommand(BaseCommand command) {
        this.getProxy().getPluginManager().unregisterCommand(command);
    }

    public void sendPlayerToServer(ProxiedPlayer player, String targetServerName) {
        ServerInfo target = getProxy().getServerInfo(targetServerName);

        if (null == target) {
            player.sendMessage(new TextComponent(ChatColor.RED + "Specified server was not found"));
            return;
        }

        PlayerServerChangeEvent event = new PlayerServerChangeEvent(player, player.getServer().getInfo(), target);
        getProxy().getPluginManager().callEvent(event);

        if (event.isCancelled()) {
            return;
        }

        if (0 > event.getDelay()) {
            event.setDelay(0L);
        }

        this.getProxy().getScheduler().schedule(this, () -> player.connect(target), event.getDelay(), TimeUnit.SECONDS);
    }
}
