package org.nocraft.loperd.serverchange.command;

import com.google.common.base.Joiner;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import org.nocraft.loperd.serverchange.ServerConnectorPlugin;

import java.util.HashSet;
import java.util.Set;

public class ServerCommand extends BaseCommand {

    private final ServerConnectorPlugin plugin;

    public ServerCommand(ServerConnectorPlugin plugin) {
        super(plugin, "server");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (!sender.hasPermission("serverconnector.server")) {
            sender.sendMessage(new TextComponent(ChatColor.RED + "You don't have permission for this command."));
            return;
        }

        if (args.length == 0) {
            String available = Joiner.on(", ").join(plugin.getProxy().getServers().keySet().toArray(new String[0]));
            StringBuilder message = new StringBuilder().append(ChatColor.GRAY).append("Available servers:");
            message.append(" ").append(ChatColor.AQUA).append(available).append(".");


            if (sender instanceof ProxiedPlayer) {
                Server server = ((ProxiedPlayer) sender).getServer();
                message.append("\n").append(ChatColor.GRAY).append("You are on the server")
                        .append(" ").append(ChatColor.GREEN).append(server.getInfo().getName());
            }

            sender.sendMessage(new TextComponent(message.toString()));
            return;
        }

        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(new TextComponent(ChatColor.RED + "You have to be a player to use this command."));
            return;
        }

        ProxiedPlayer p = (ProxiedPlayer) sender;

        plugin.sendPlayerToServer(p, args[0]);
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        Set<String> result = new HashSet<>();
        if (0 == args.length) {
            return result;
        }
        for (String s : plugin.getProxy().getServers().keySet()) {
            if (s.startsWith(args[0])) {
                result.add(s);
            }
        }
        return result;
    }
}
