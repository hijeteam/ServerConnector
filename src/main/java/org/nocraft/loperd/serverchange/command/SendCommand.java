package org.nocraft.loperd.serverchange.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import org.nocraft.loperd.serverchange.ServerConnectorPlugin;

public class SendCommand extends BaseCommand {

    public SendCommand(ServerConnectorPlugin plugin) {
        super(plugin, "send");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (0 == args.length) {
            sender.sendMessage(new TextComponent(ChatColor.RED + "Usage /send <server|player|all|current> <server|player>"));
            return;
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        return null;
    }
}
